package app.view.panels;

import java.awt.Desktop;
import java.awt.Font;
import java.net.URI;

import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLDocument;

public class ErrorMessagePane extends JEditorPane {
	
	public ErrorMessagePane(String uri) {
		super("text/html", "<html><body>Error occured during server sign in,<br>please visit <a href=\""+uri+"\">link</a> for more information.</body></html>");
		JLabel label = new JLabel();

		addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)){
					Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
					if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
						try {
							desktop.browse(URI.create(uri));
						} catch (Exception exception) {
							JOptionPane.showMessageDialog(null, "Do\u0161lo je do gre\u0161ke!", "Gre\u0161ka",
									JOptionPane.WARNING_MESSAGE);
						}
					}
				}
					
			}
		});

		Font f = UIManager.getFont("Label.font");
		String bodyRule = "body { font-family: " + f.getFamily() + "; " + "font-size: " + f.getSize() + "pt; }";
		((HTMLDocument) getDocument()).getStyleSheet().addRule(bodyRule);

		setOpaque(false);
		setBorder(null);
		setFont(label.getFont());
		setEditable(false);
		setBackground(label.getBackground());
	}
}
