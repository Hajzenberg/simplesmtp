package app.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import app.Application;
import app.enums.SecurityType;
import app.model.Context;
import app.util.Util;
import app.view.panels.SendPanel;

public class SettingsDialog extends JDialog {

	public SettingsDialog() {

		setTitle("Settings");
		setModalityType(ModalityType.APPLICATION_MODAL);
		// addWindowListener(new LogInAdapter());
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setIconImage(Util.loadIcon(getClass(), "key24.png").getImage());

		initUI();

		pack();
		setLocationRelativeTo(null);

		setVisible(true);

	}

	private void initUI() {
		Context cnt = Application.getInstance().getContext();
		
		JPanel rootPanel = new JPanel();

		rootPanel.setLayout(new BorderLayout());

		add(rootPanel);

		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new GridLayout(7, 1));

		// name

		JPanel namePanel = new JPanel();

		JLabel nameLabel = new JLabel("Name:");
		nameLabel.setPreferredSize(new Dimension(120, 26));
		JTextField nameField = new JTextField(30);
		nameField.setText(cnt.getName());

		namePanel.add(nameLabel);
		namePanel.add(nameField);

		inputPanel.add(namePanel);

		// mail

		JPanel mailPanel = new JPanel();

		JLabel mailLabel = new JLabel("Email adress:");
		mailLabel.setPreferredSize(new Dimension(120, 26));
		JTextField mailField = new JTextField(30);
		mailField.setText(cnt.getEmailAdress());

		mailPanel.add(mailLabel);
		mailPanel.add(mailField);

		inputPanel.add(mailPanel);

		// pass

		JPanel passPanel = new JPanel();

		JLabel passLabel = new JLabel("Password: ");
		passLabel.setPreferredSize(new Dimension(120, 26));
		JTextField passField = new JTextField(30);
		passField.setText(cnt.getPassword());

		passPanel.add(passLabel);
		passPanel.add(passField);

		inputPanel.add(passPanel);

		// server

		JPanel serverPanel = new JPanel();

		JLabel serverLabel = new JLabel("SMTP Server: ");
		passLabel.setPreferredSize(new Dimension(120, 26));
		JTextField serverField = new JTextField(30);
		serverField.setText(cnt.getServer());

		serverPanel.add(serverLabel);
		serverPanel.add(serverField);

		inputPanel.add(serverPanel);

		// port

		JPanel portPanel = new JPanel();

		JLabel portLabel = new JLabel("Port: ");
		portLabel.setPreferredSize(new Dimension(120, 26));
		JTextField portField = new JTextField(30);
		portField.setText(String.valueOf(cnt.getPortNumber()));

		portPanel.add(portLabel);
		portPanel.add(portField);

		inputPanel.add(portPanel);

		// checkboxes

		JPanel checkPanel = new JPanel();

		JRadioButton sslRadio = new JRadioButton("SSL Enabled");
		checkPanel.add(sslRadio);

		JRadioButton tlsRadio = new JRadioButton("STARTTLS Enabled");
		checkPanel.add(tlsRadio);

		ButtonGroup radioGroup = new ButtonGroup();
		radioGroup.add(sslRadio);
		radioGroup.add(tlsRadio);
		
		if (cnt.getSecurityType() == SecurityType.SSL){
			sslRadio.setSelected(true);
		} else {
			tlsRadio.setSelected(true);
		}

		inputPanel.add(checkPanel);

		// buttons

		JPanel buttonPanel = new JPanel();

		JButton okButton = new JButton(new AbstractAction() {

			{
				putValue(NAME, "OK");
			}

			@Override
			public void actionPerformed(ActionEvent e) {
				
				int port;
				
				if (!portField.getText().matches("^\\d+$")){
					port = 0;
				} else {
					port = Integer.parseInt(portField.getText());
				}
				
				Application.getInstance().getContext().setName(nameField.getText());
				Application.getInstance().getContext().setEmailAdress(mailField.getText());
				Application.getInstance().getContext().setPassword(passField.getText());
				Application.getInstance().getContext().setServer(serverField.getText());
				Application.getInstance().getContext().setPortNumber(port);
				Application.getInstance().getContext().setSecurityType(sslRadio.isSelected()?SecurityType.SSL:SecurityType.STARTTLS);
				((SendPanel)Application.getInstance().getClientFrame().getSplitPane().getRightComponent()).getFromField().setText(mailField.getText());
				
				SettingsDialog.this.dispose();
			}
		});

		JButton cancelButton = new JButton(new AbstractAction() {

			{
				putValue(NAME, "Cancel");
			}

			@Override
			public void actionPerformed(ActionEvent e) {
				SettingsDialog.this.dispose();

			}
		});

		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);

		inputPanel.add(buttonPanel);

		add(inputPanel, BorderLayout.NORTH);
	}

}
