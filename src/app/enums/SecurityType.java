package app.enums;

public enum SecurityType {
	SSL, STARTTLS, PLAIN_SMTP
}
