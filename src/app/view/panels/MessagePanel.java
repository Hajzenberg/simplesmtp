package app.view.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import app.Application;
import app.model.Message;
import app.util.Util;

public class MessagePanel extends JPanel {

	private JTextArea mEditArea;

	public MessagePanel(boolean isEditable, Message message) {

		initUI(isEditable, message);
	}

	private void initUI(boolean isEditable, Message message) {
		setLayout(new BorderLayout(0,0)); // vgap, hgap
		add(new JPanel(), BorderLayout.EAST);
		add(new JPanel(), BorderLayout.WEST);
		setPreferredSize(new Dimension(600, 200));
		initInfoPanel(message);
		initEditArea(isEditable, message.getContent());
		initButtonPanel(isEditable);
	}

	private void initInfoPanel(Message message) {

		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new GridLayout(4, 1));

		// from

		JPanel fromPanel = new JPanel();

		JLabel fromLabel = new JLabel("From:");
		fromLabel.setPreferredSize(new Dimension(120, 26));
		JTextField fromLabelValue = new JTextField(30);
		fromLabelValue.setText(message.getFrom());
		fromLabelValue.setEditable(false);

		fromPanel.add(fromLabel);
		fromPanel.add(fromLabelValue);

		infoPanel.add(fromPanel);

		// to

		JPanel toPanel = new JPanel();

		JLabel toLabel = new JLabel("To:");
		toLabel.setPreferredSize(new Dimension(120, 26));

		String to = "";

		for (String s : message.getTo()) {
			to = to + s + ";";
		}

		JTextField toLabelValue = new JTextField(30);
		toLabelValue.setEditable(false);
		toLabelValue.setText(to);

		toPanel.add(toLabel);
		toPanel.add(toLabelValue);

		infoPanel.add(toPanel);

		// date

		JPanel datePanel = new JPanel();

		JLabel dateLabel = new JLabel("Date: ");
		dateLabel.setPreferredSize(new Dimension(120, 26));
		JTextField dateLabelValue = new JTextField(30);
		dateLabelValue.setText(message.getDate());
		dateLabelValue.setEditable(false);

		datePanel.add(dateLabel);
		datePanel.add(dateLabelValue);

		infoPanel.add(datePanel);

		// subject

		JPanel subjectPanel = new JPanel();

		JLabel subjectLabel = new JLabel("Subject: ");
		subjectLabel.setPreferredSize(new Dimension(120, 26));
		JTextField subjectLabelValue = new JTextField(30);
		subjectLabelValue.setText(message.getSubject());
		subjectLabelValue.setEditable(false);

		subjectPanel.add(subjectLabel);
		subjectPanel.add(subjectLabelValue);

		infoPanel.add(subjectPanel);

		add(infoPanel, BorderLayout.NORTH);

	}

	private void initEditArea(boolean isEditable, String message) {
		
		JPanel messagePanel = new JPanel();
		
		mEditArea = new JTextArea();
		mEditArea.setEditable(isEditable);
		mEditArea.setText(message);
		mEditArea.setFont(new JTextField().getFont());
		mEditArea.setPreferredSize(new Dimension(621, 600));
		mEditArea.setBorder(BorderFactory.createLineBorder(new Color(25, 25, 112), 1));

		JScrollPane scrollPane = new JScrollPane(mEditArea);
		
		messagePanel.add(scrollPane);
		add(messagePanel);
	}

	private void initButtonPanel(boolean isEditable) {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		JButton okButton = new JButton(new AbstractAction("OK") {

			@Override
			public void actionPerformed(ActionEvent e) {

				Application.getInstance().getClientFrame().showMessages();
			}
		});

		buttonPanel.add(okButton);

		add(buttonPanel, BorderLayout.SOUTH);
	}
}
