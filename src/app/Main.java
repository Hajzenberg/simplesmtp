package app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Base64;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.swing.SwingUtilities;

public class Main {
	
	/* SOURCES
	 * KOSTUR - https://tools.ietf.org/html/rfc5321
	 * AUTENTIFIKACIJA - https://tools.ietf.org/html/rfc4954
	 * STARTTLS - https://tools.ietf.org/html/rfc3207
	 * UKRATKO - http://www.samlogic.net/articles/smtp-commands-reference.htm
	 * GOOGLE LESS SECURITY - https://myaccount.google.com/lesssecureapps?pli=1
	 */
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				Application.getInstance();
			}
		});

	}

}
