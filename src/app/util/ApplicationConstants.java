package app.util;

import java.util.regex.Pattern;

public class ApplicationConstants {
	public static final Pattern VALID_EMAIL_ADDRESS_PATTERN = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	
	public static final String VALID_EMAIL_ADDRESS_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public static final int SLEEP_TIME = 500;
}
