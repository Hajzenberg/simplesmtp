package app.util;

import javax.swing.ImageIcon;

public class Util {

	public static String PACKAGE_PATH_ICON = "/res/icons/";
	public static String PACKAGE_PATH_IMAGE = "/res/images/";

	/* Pozivate kao Util.loadIcon(getClass(), "ime.png"); */

	public static <T> ImageIcon loadIcon(Class<T> context, String name) {

		return new ImageIcon(context.getResource(PACKAGE_PATH_ICON.concat(name)));
	}

	public static <T> ImageIcon loadImage(Class<T> context, String name) {

		return new ImageIcon(context.getResource(PACKAGE_PATH_IMAGE.concat(name)));
	}
}
