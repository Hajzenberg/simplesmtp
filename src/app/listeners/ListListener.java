package app.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JList;
import javax.swing.SwingUtilities;

import app.Application;
import app.model.Message;

public class ListListener implements MouseListener {

	private JList<Message> mList;

	public ListListener(JList<Message> list) {
		mList = list;
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		if (e.getClickCount() > 1) {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					Application.getInstance().getClientFrame().showMessage(mList.getSelectedValue());

				}
			});
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
