package app.controller;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;

import app.util.Util;
import app.view.SettingsDialog;

public class SettingsController extends AbstractAction {

	public SettingsController() {
		putValue(NAME, "Settings");
		putValue(SMALL_ICON, Util.loadIcon(getClass(), "settings32.png"));
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new SettingsDialog();
				
			}
		});

	}

}
