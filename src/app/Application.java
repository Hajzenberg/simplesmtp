package app;

import java.io.File;
import java.io.IOException;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.fasterxml.jackson.databind.ObjectMapper;

import app.model.Context;
import app.view.ClientFrame;
import app.view.panels.SendPanel;


public class Application {
	
	private static Application mInstance;
	private Context mContext;
	private ClientFrame mClientFrame;

	private Application() {

		setLookAndFeel();
		//WebLookAndFeel.install();
		
		File contextFile = new File(Context.CONTEXT_PATH);

		if (contextFile.exists()) {

			ObjectMapper mapper = new ObjectMapper();

			try {
				mContext = mapper.readValue(contextFile, Context.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			mContext = new Context();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				mClientFrame = new ClientFrame();
				((SendPanel)mClientFrame.getSplitPane().getRightComponent()).getFromField().setText(mContext.getEmailAdress());	
			}
		});
	}
	
	public static Application getInstance() {
		if (mInstance == null) {
			mInstance = new Application();
		}
		return mInstance;
	}
	
	private void setLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
	}

	public Context getContext() {
		return mContext;
	}

	public ClientFrame getClientFrame() {
		return mClientFrame;
	}
}
