package app.controller.send;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Base64;
import java.util.regex.Matcher;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import app.Application;
import app.controller.send.threads.SSLSendThread;
import app.controller.send.threads.STARTTSLSendThread;
import app.enums.SecurityType;
import app.model.Context;
import app.model.Message;
import app.util.ApplicationConstants;
import app.util.Util;
import app.view.dialogs.DebugDialog;
import app.view.panels.ErrorMessagePane;
import app.view.panels.SendPanel;

public class SendController extends AbstractAction {

	protected DefaultListModel<Message> mSentTabelModel;
	protected SendPanel mSendPanel;

	public SendController(DefaultListModel<Message> model, SendPanel sendPanel) {
		putValue(NAME, "Send");
		putValue(SMALL_ICON, Util.loadIcon(getClass(), "success24.png"));
		putValue(LARGE_ICON_KEY, Util.loadIcon(getClass(), "success32.png"));
		putValue(SHORT_DESCRIPTION, "Send Mail");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		putValue(MNEMONIC_KEY, KeyEvent.VK_S);
		mSentTabelModel = model;
		mSendPanel = sendPanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (!isInputValid()) {
			JOptionPane.showMessageDialog(null, "Enter valid email adresses!", "Error", JOptionPane.WARNING_MESSAGE);
			return;
		}

		Context cnt = Application.getInstance().getContext();

		Message message = new Message(mSendPanel.getFromField().getText(), mSendPanel.getToField().getText(),
				mSendPanel.getSubjectField().getText(), mSendPanel.getTextArea().getText());

		if (cnt.getSecurityType() == SecurityType.SSL) {
			new SSLSendThread(mSentTabelModel, mSendPanel, Application.getInstance().getContext(), message,
					Application.getInstance().getContext().isInDebugMode()).start();
		} else {
			new STARTTSLSendThread(mSentTabelModel, mSendPanel, Application.getInstance().getContext(), message,
					Application.getInstance().getContext().isInDebugMode()).start();
		}
	}

	protected boolean isInputValid() {
		
		String from = mSendPanel.getFromField().getText();
		
		if (from.equals("")){
			return false;
		}
		
		if (!from.matches(ApplicationConstants.VALID_EMAIL_ADDRESS_REGEX)) {
			return false;
		}

		String to = mSendPanel.getToField().getText();

		if (to.equals("")) {
			return false;
		}

		String[] adresses = to.split(";");

		for (String string : adresses) {
			if (!string.matches(ApplicationConstants.VALID_EMAIL_ADDRESS_REGEX)) {
				return false;
			}
		}

		return true;
	}

}
