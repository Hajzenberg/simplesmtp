package app.controller.send;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Base64;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

import app.Application;
import app.model.Context;
import app.model.Message;
import app.view.panels.ErrorMessagePane;
import app.view.panels.SendPanel;

public class PlainSendController extends SendController {

	public PlainSendController(DefaultListModel<Message> model, SendPanel sendPanel) {
		super(model, sendPanel);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		Context cnt = Application.getInstance().getContext();

		Message message = new Message(mSendPanel.getFromField().getText(), mSendPanel.getToField().getText(),
				mSendPanel.getSubjectField().getText(), mSendPanel.getTextArea().getText());

		sendOverSSL(message, cnt);
	}

	private void sendOverSSL(Message message, Context cnt) {

		SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();

		try {
			Socket socket = new Socket(cnt.getServer(), cnt.getPortNumber());

			PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			//boolean failLogin = false;

			String inMessage;
			String outMessage;
			//String httpLink = null;

			inMessage = in.readLine();
			showIncoming(in, inMessage);

			outMessage = "HELO " + socket.getLocalAddress().getHostAddress();
			showOutgoing(out, outMessage);

			inMessage = in.readLine();
			showIncoming(in, inMessage);

//			outMessage = "AUTH LOGIN";
//			showOutgoing(out, outMessage);
//
//			inMessage = in.readLine();
//			showIncoming(in, inMessage);
//
//			byte[] encodedUsername = Base64.getEncoder().encode(cnt.getEmailAdress().getBytes());
//			String username = new String(encodedUsername);
//
//			outMessage = username;
//			showOutgoing(out, outMessage);
//
//			inMessage = in.readLine();
//			showIncoming(in, inMessage);
//
//			byte[] encodedPassword = Base64.getEncoder().encode(cnt.getPassword().getBytes());
//			String password = new String(encodedPassword);
//
//			outMessage = password;
//			showOutgoing(out, outMessage);
//
//			do {
//				inMessage = in.readLine();
//				showIncoming(in, inMessage);
//
//				if (inMessage.startsWith("535") || inMessage.startsWith("534")) {
//					failLogin = true;
//					httpLink = checkForHTTP(inMessage);
//				}
//
//			} while (inMessage.charAt(3) == '-');
//
//			if (failLogin) {
//				closePlain(in, out, socket);
//
//				JOptionPane.showMessageDialog(null, new ErrorMessagePane(httpLink), "Log in fail!",
//						JOptionPane.WARNING_MESSAGE);
//
//				return;
//			}

			outMessage = "MAIL FROM: <" + message.getFrom() + ">";
			showOutgoing(out, outMessage);

			do {
				inMessage = in.readLine();
				showIncoming(in, inMessage);
			} while (inMessage.charAt(3) == '-');

			for (String s : message.getTo()) {
				outMessage = "RCPT TO: <" + s + ">";
				showOutgoing(out, outMessage);
				do {
					inMessage = in.readLine();
					showIncoming(in, inMessage);
				} while (inMessage.charAt(3) == '-');
			}

			outMessage = "DATA";
			showOutgoing(out, outMessage);

			inMessage = in.readLine();
			showIncoming(in, inMessage);

			// TEKST PORUKE

			outMessage = "From: " + cnt.getName() + " <" + cnt.getEmailAdress() + ">";
			showOutgoing(out, outMessage);

			for (String s : message.getTo()) {
				outMessage = "To: <" + s + ">";
				showOutgoing(out, outMessage);
			}

			outMessage = "Subject: " + message.getSubject();
			showOutgoing(out, outMessage);

			outMessage = message.getContent();
			showOutgoing(out, outMessage);

			outMessage = ".";
			showOutgoing(out, outMessage);

			inMessage = in.readLine();
			showIncoming(in, inMessage);

			closePlain(in, out, socket);

			resetView(message);

		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	private void closePlain(BufferedReader in, PrintWriter out, Socket sslsocket) {
		try {

			String outMessage = "QUIT";
			showOutgoing(out, outMessage);

			String inMessage = in.readLine();
			showIncoming(in, inMessage);

			in.close();
			out.close();
			sslsocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void resetView(Message message) {
		mSentTabelModel.add(0, message);
		mSendPanel.getToField().setText("");
		mSendPanel.getSubjectField().setText("");
		mSendPanel.getTextArea().setText("");
	}

	protected void showOutgoing(PrintWriter out, String outMessage) {
		out.println(outMessage);
		System.out.println("[C]: " + outMessage);
	}

	protected void showIncoming(BufferedReader in, String inMessage) {
		System.out.println("[S]: " + inMessage);
	}
	
	protected String checkForHTTP(String string){
		
		if (!string.contains("http")){
			return null;
		}
		
		String s[] = string.split(" ");
		
		for (String str : s) {
			if (str.contains("http")){
				return str;
			}
		}
		
		return null;
	}

}
