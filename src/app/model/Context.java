package app.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import app.enums.SecurityType;

public class Context {
	
	public static final String CONTEXT_DIRECTORY = System.getProperty("user.home");
	public static final String CONTEXT_FILE_NAME = "smtpcontext.json";
	public static final String CONTEXT_PATH = CONTEXT_DIRECTORY + "\\" + CONTEXT_FILE_NAME;
	
	@JsonProperty("sent")
	private ArrayList<Message> mSent;
	@JsonProperty("name")
	private String mName;
	@JsonProperty("email")
	private String mEmailAdress;
	@JsonProperty("password")
	private String mPassword;
	@JsonProperty("server")
	private String mServer;
	@JsonProperty("security")
	private SecurityType mSecurityType;
	@JsonProperty("port")
	private int mPortNumber;
	@JsonIgnore
	private boolean mInDebugMode;
	
	public Context() {
		mSent = new ArrayList<>();
	}

	public ArrayList<Message> getSent() {
		return mSent;
	}

	public void setSent(ArrayList<Message> mSent) {
		this.mSent = mSent;
	}

	public String getName() {
		return mName;
	}

	public void setName(String mName) {
		this.mName = mName;
	}

	public String getEmailAdress() {
		return mEmailAdress;
	}

	public void setEmailAdress(String mEmailAdress) {
		this.mEmailAdress = mEmailAdress;
	}

	public String getPassword() {
		return mPassword;
	}

	public void setPassword(String mPassword) {
		this.mPassword = mPassword;
	}

	public String getServer() {
		return mServer;
	}

	public void setServer(String mServer) {
		this.mServer = mServer;
	}

	public SecurityType getSecurityType() {
		return mSecurityType;
	}

	public void setSecurityType(SecurityType mSecurityType) {
		this.mSecurityType = mSecurityType;
	}

	public int getPortNumber() {
		return mPortNumber;
	}

	public void setPortNumber(int mPortNumber) {
		this.mPortNumber = mPortNumber;
	}

	public boolean isInDebugMode() {
		return mInDebugMode;
	}

	public void setInDebugMode(boolean mInDebugMode) {
		this.mInDebugMode = mInDebugMode;
	}

}
