package app.controller.send.threads;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Base64;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

import app.Application;
import app.model.Context;
import app.model.Message;
import app.util.ApplicationConstants;
import app.view.dialogs.DebugDialog;
import app.view.panels.ErrorMessagePane;
import app.view.panels.SendPanel;

public class STARTTSLSendThread extends Thread {
	
	private SendPanel mSendPanel;
	private DefaultListModel<Message> mSentTabelModel;
	private Context mContext;
	private Message mMessage;
	private DebugDialog mDebugDialog;
	private boolean mIsInDebug;

	public STARTTSLSendThread(DefaultListModel<Message> model, SendPanel sendPanel, Context context, Message message,
			boolean isInDebug) {
		mSendPanel = sendPanel;
		mSentTabelModel = model;
		mContext = context;
		mMessage = message;
		mIsInDebug = isInDebug;
	}

	@Override
	public void run() {
		super.run();
		sendOverSTARTTSL();
	}
	
	private void sendOverSTARTTSL() {
		
		mSendPanel.getSendButton().setEnabled(false);

		try {
			Socket socket = new Socket(mContext.getServer(), mContext.getPortNumber());
			SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
			
			if (mIsInDebug) {
				mDebugDialog = new DebugDialog(false);
			}

			PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			boolean failLogin = false;

			String inMessage;
			String outMessage;
			String httpLink = null;
			
			inMessage = in.readLine();
			
			if (inMessage == null){
				preemptCloseSTARTTLS(in, out, socket);
				JOptionPane.showMessageDialog(null, "Connection error, check settings!", "Error!",
						JOptionPane.WARNING_MESSAGE);
				mSendPanel.getSendButton().setEnabled(true);
				return;
			}
			
			showIncoming(in, inMessage);

			outMessage = "EHLO " + socket.getLocalAddress().getHostAddress();
			showOutgoing(out, outMessage);

			do {
				inMessage = in.readLine();
				showIncoming(in, inMessage);
			} while (inMessage.charAt(3) == '-');

			outMessage = "STARTTLS";
			showOutgoing(out, outMessage);

			inMessage = in.readLine();
			showIncoming(in, inMessage);

			SSLSocket sslsocket = (SSLSocket) sslsocketfactory.createSocket(socket, mContext.getServer(),
					mContext.getPortNumber(), false);

			sslsocket.startHandshake();

			PrintWriter sout = new PrintWriter(new OutputStreamWriter(sslsocket.getOutputStream()), true);
			BufferedReader sin = new BufferedReader(new InputStreamReader(sslsocket.getInputStream()));
			
			outMessage = "EHLO " + sslsocket.getLocalAddress().getHostAddress();
			showOutgoing(sout, outMessage);

			do {
				inMessage = sin.readLine();
				showIncoming(sin, inMessage);
			} while (inMessage.charAt(3) == '-');

			outMessage = "AUTH LOGIN";
			showOutgoing(sout, outMessage);

			inMessage = sin.readLine();
			showIncoming(sin, inMessage);

			byte[] encodedUsername = Base64.getEncoder().encode(mContext.getEmailAdress().getBytes());
			String username = new String(encodedUsername);

			outMessage = username;
			showOutgoing(sout, outMessage);

			inMessage = sin.readLine();
			showIncoming(sin, inMessage);

			byte[] encodedPassword = Base64.getEncoder().encode(mContext.getPassword().getBytes());
			String password = new String(encodedPassword);

			outMessage = password;
			showOutgoing(sout, outMessage);

			do {
				inMessage = sin.readLine();
				showIncoming(sin, inMessage);

				if (inMessage.startsWith("535") || inMessage.startsWith("534")) {
					failLogin = true;
					httpLink = checkForHTTP(inMessage);
				}

			} while (inMessage.charAt(3) == '-');

			if (failLogin) {
				closeSTARTTLS(in, sin, out, sout, socket, sslsocket);
				mSendPanel.getSendButton().setEnabled(true);
				JOptionPane.showMessageDialog(null, new ErrorMessagePane(httpLink), "Log in fail!",
						JOptionPane.WARNING_MESSAGE);

				return;
			}

			outMessage = "MAIL FROM: <" + mMessage.getFrom() + ">";
			showOutgoing(sout, outMessage);

			do {
				inMessage = sin.readLine();
				showIncoming(sin, inMessage);
			} while (inMessage.charAt(3) == '-');

			for (String s : mMessage.getTo()) {
				outMessage = "RCPT TO: <" + s + ">";
				showOutgoing(sout, outMessage);
				do {
					inMessage = sin.readLine();
					showIncoming(sin, inMessage);
				} while (inMessage.charAt(3) == '-');
			}

			outMessage = "DATA";
			showOutgoing(sout, outMessage);

			inMessage = sin.readLine();
			showIncoming(sin, inMessage);

			// TEKST PORUKE
			
			outMessage = "From: "+mContext.getName()+" <"+mContext.getEmailAdress()+">";
			showOutgoing(sout, outMessage);
			
			for (String s : mMessage.getTo()) {
				outMessage = "To: <"+s+">";
				showOutgoing(sout, outMessage);
			}
			
			outMessage = "Date: "+mMessage.getDate();
			showOutgoing(sout, outMessage);
			
			outMessage = "Subject: "+mMessage.getSubject();
			showOutgoing(sout, outMessage);

			outMessage = mMessage.getContent();
			showOutgoing(sout, outMessage);

			outMessage = ".";
			showOutgoing(sout, outMessage);

			inMessage = sin.readLine();
			showIncoming(sin, inMessage);

			closeSTARTTLS(in, sin, out, sout, socket, sslsocket);
			
			resetView(mMessage);

		} catch (IOException e) {
			//e.printStackTrace();
			mSendPanel.getSendButton().setEnabled(true);
			JOptionPane.showMessageDialog(null, "Connection error, check settings!", "Error!",
					JOptionPane.WARNING_MESSAGE);
		}

	}
	
	private void closeSTARTTLS(BufferedReader in, BufferedReader sin, PrintWriter out, PrintWriter sout, Socket socket, SSLSocket sslsocket){
		try {
			String outMessage = "QUIT";
			showOutgoing(sout, outMessage);

			String inMessage = sin.readLine();
			showIncoming(sin, inMessage);
			
			in.close();
			out.close();
			sin.close();
			sout.close();
			socket.close();
			sslsocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	private void preemptCloseSTARTTLS(BufferedReader in, PrintWriter out,Socket socket){
		try {			
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	protected void resetView(Message message) {
		mSentTabelModel.add(0, message);
		mSendPanel.getToField().setText("");
		mSendPanel.getSubjectField().setText("");
		mSendPanel.getTextArea().setText("");
		mSendPanel.getSendButton().setEnabled(true);
	}

	protected void showOutgoing(PrintWriter out, String outMessage) {
		out.println(outMessage);
		//System.out.println("[C]: " + outMessage);
		if (mIsInDebug){
			mDebugDialog.update("[C]: " + outMessage);
			
			try {
				sleep(ApplicationConstants.SLEEP_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		

	}

	protected void showIncoming(BufferedReader in, String inMessage) {
		//System.out.println("[S]: " + inMessage);
		
		if (mIsInDebug){
			mDebugDialog.update("[S]: " + inMessage);
			
			try {
				sleep(ApplicationConstants.SLEEP_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	protected String checkForHTTP(String string){
		
		if (!string.contains("http")){
			return null;
		}
		
		String s[] = string.split(" ");
		
		for (String str : s) {
			if (str.contains("http")){
				return str;
			}
		}
		
		return null;
	}

}
