package app.controller.send.threads;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Base64;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import app.Application;
import app.model.Context;
import app.model.Message;
import app.util.ApplicationConstants;
import app.view.dialogs.DebugDialog;
import app.view.panels.ErrorMessagePane;
import app.view.panels.SendPanel;

public class SSLSendThread extends Thread {

	private SendPanel mSendPanel;
	private DefaultListModel<Message> mSentTabelModel;
	private Context mContext;
	private Message mMessage;
	private DebugDialog mDebugDialog;
	private boolean mIsInDebug;

	public SSLSendThread(DefaultListModel<Message> model, SendPanel sendPanel, Context context, Message message,
			boolean isInDebug) {
		mSendPanel = sendPanel;
		mSentTabelModel = model;
		mContext = context;
		mMessage = message;
		mIsInDebug = isInDebug;
	}

	@Override
	public void run() {
		super.run();
		sendOverSSL();
	}

	private void sendOverSSL() {
		
		mSendPanel.getSendButton().setEnabled(false);

		SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();

		try {
			SSLSocket sslsocket = (SSLSocket) sslsocketfactory.createSocket(mContext.getServer(),
					mContext.getPortNumber());

			if (mIsInDebug) {
				mDebugDialog = new DebugDialog(false);
			}

			sslsocket.startHandshake();

			PrintWriter out = new PrintWriter(new OutputStreamWriter(sslsocket.getOutputStream()), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(sslsocket.getInputStream()));

			boolean fallback = false;
			boolean failLogin = false;

			String inMessage;
			String outMessage;
			String httpLink = null;

			inMessage = in.readLine();
			showIncoming(in, inMessage);

			outMessage = "EHLO " + sslsocket.getLocalAddress().getHostAddress();
			showOutgoing(out, outMessage);

			do {
				inMessage = in.readLine();
				showIncoming(in, inMessage);

				/*
				 * http://www.ietf.org/mail-archive/web/ietf-smtp/current/
				 * msg05481.html
				 */

				if (inMessage.startsWith("502")) {
					fallback = true;
				}
			} while (inMessage.charAt(3) == '-');

			if (fallback) {
				outMessage = "HELO " + sslsocket.getLocalAddress().getHostAddress();
				showOutgoing(out, outMessage);

				inMessage = in.readLine();
				showIncoming(in, inMessage);
			}

			outMessage = "AUTH LOGIN";
			showOutgoing(out, outMessage);

			inMessage = in.readLine();
			showIncoming(in, inMessage);

			byte[] encodedUsername = Base64.getEncoder().encode(mContext.getEmailAdress().getBytes());
			String username = new String(encodedUsername);

			outMessage = username;
			showOutgoing(out, outMessage);

			inMessage = in.readLine();
			showIncoming(in, inMessage);

			byte[] encodedPassword = Base64.getEncoder().encode(mContext.getPassword().getBytes());
			String password = new String(encodedPassword);

			outMessage = password;
			showOutgoing(out, outMessage);

			do {
				inMessage = in.readLine();
				showIncoming(in, inMessage);

				if (inMessage.startsWith("535") || inMessage.startsWith("534")) {
					failLogin = true;
					httpLink = checkForHTTP(inMessage);
				}

			} while (inMessage.charAt(3) == '-');

			if (failLogin) {
				closeSSL(in, out, sslsocket);
				mSendPanel.getSendButton().setEnabled(true);
				JOptionPane.showMessageDialog(null, new ErrorMessagePane(httpLink), "Log in fail!",
						JOptionPane.WARNING_MESSAGE);

				return;
			}

			outMessage = "MAIL FROM: <" + mMessage.getFrom() + ">";
			showOutgoing(out, outMessage);

			do {
				inMessage = in.readLine();
				showIncoming(in, inMessage);

			} while (inMessage.charAt(3) == '-');

			for (String s : mMessage.getTo()) {
				outMessage = "RCPT TO: <" + s + ">";
				showOutgoing(out, outMessage);
				do {
					inMessage = in.readLine();
					showIncoming(in, inMessage);
				} while (inMessage.charAt(3) == '-');
			}

			outMessage = "DATA";
			showOutgoing(out, outMessage);

			inMessage = in.readLine();
			showIncoming(in, inMessage);

			// TEKST PORUKE

			outMessage = "From: " + mContext.getName() + " <" + mContext.getEmailAdress() + ">";
			showOutgoing(out, outMessage);

			for (String s : mMessage.getTo()) {
				outMessage = "To: <" + s + ">";
				showOutgoing(out, outMessage);
			}

			outMessage = "Date: " + mMessage.getDate();
			showOutgoing(out, outMessage);
			
			outMessage = "Subject: " + mMessage.getSubject();
			showOutgoing(out, outMessage);

			outMessage = mMessage.getContent();
			showOutgoing(out, outMessage);

			outMessage = ".";
			showOutgoing(out, outMessage);

			inMessage = in.readLine();
			showIncoming(in, inMessage);

			closeSSL(in, out, sslsocket);

			resetView(mMessage);

		} catch (IOException exception) {
			//exception.printStackTrace();
			mSendPanel.getSendButton().setEnabled(true);
			JOptionPane.showMessageDialog(null, "Connection error, check settings!", "Error!",
					JOptionPane.WARNING_MESSAGE);
		}
	}

	private void closeSSL(BufferedReader in, PrintWriter out, SSLSocket sslsocket) {
		try {
			String outMessage = "QUIT";
			showOutgoing(out, outMessage);

			String inMessage = in.readLine();
			showIncoming(in, inMessage);

			in.close();
			out.close();
			sslsocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void resetView(Message message) {
		mSentTabelModel.add(0, message);
		mSendPanel.getToField().setText("");
		mSendPanel.getSubjectField().setText("");
		mSendPanel.getTextArea().setText("");
		mSendPanel.getSendButton().setEnabled(true);
	}

	protected void showOutgoing(PrintWriter out, String outMessage) {
		out.println(outMessage);
		//System.out.println("[C]: " + outMessage);
		if (mIsInDebug){
			mDebugDialog.update("[C]: " + outMessage);
			
			try {
				sleep(ApplicationConstants.SLEEP_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	protected void showIncoming(BufferedReader in, String inMessage) {
		//System.out.println("[S]: " + inMessage);
		if (mIsInDebug){
			mDebugDialog.update("[S]: " + inMessage);
			
			try {
				sleep(ApplicationConstants.SLEEP_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	protected String checkForHTTP(String string) {

		if (!string.contains("http")) {
			return null;
		}

		String s[] = string.split(" ");

		for (String str : s) {
			if (str.contains("http")) {
				return str;
			}
		}

		return null;
	}

}
