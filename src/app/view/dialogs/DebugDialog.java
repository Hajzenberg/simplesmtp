package app.view.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import app.Application;
import app.model.Message;
import app.util.Util;

public class DebugDialog extends JDialog {

	private final static int ROWS = 45;
	private final static int COLUMNS = 50;

	private JTextArea mEditArea;

	public DebugDialog(boolean isEditable) {

		initUI(isEditable);
		setTitle("SMTP DEBUG - SECURITY TYPE: "+Application.getInstance().getContext().getSecurityType());
		setModalityType(ModalityType.MODELESS);
		setIconImage(Util.loadIcon(getClass(), "debug32.png").getImage());
		pack();
		setVisible(true);
	}

	private void initUI(boolean isEditable) {
		JPanel dialogPanel = new JPanel();
		dialogPanel.setLayout(new BorderLayout()); // vgap, hgap

		initEditArea(dialogPanel, isEditable);
		initButtonPanel(dialogPanel, isEditable);

		add(dialogPanel);
		pack();
		setLocationRelativeTo(null);
	}

	private void initEditArea(JPanel dialogPanel, boolean isEditable) {
		mEditArea = new JTextArea(ROWS, COLUMNS);
		mEditArea.setEditable(isEditable);
		mEditArea.setText("");
		mEditArea.setFont(new JTextField().getFont());

		JScrollPane scrollPane = new JScrollPane(mEditArea);
		dialogPanel.add(scrollPane, BorderLayout.CENTER);
	}

	private void initButtonPanel(JPanel dialogPanel, boolean isEditable) {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		JButton cancelButton = new JButton(new AbstractAction("OK") {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		buttonPanel.add(cancelButton);

		dialogPanel.add(buttonPanel, BorderLayout.SOUTH);
	}

	public JTextArea getEditArea() {
		return mEditArea;
	}
	
	public void update(String message){
		//System.out.println(SwingUtilities.isEventDispatchThread());
		mEditArea.append(message+"\n");
		//revalidate();
		//repaint();
	}
}
