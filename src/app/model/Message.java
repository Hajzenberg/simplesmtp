package app.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {
	
	@JsonProperty("metaPath")
	private String mFrom;
	@JsonProperty("to")
	private ArrayList<String> mTo;
	@JsonProperty("date")
	private String mDate;
	@JsonProperty("subject")
	private String mSubject;
	@JsonProperty("content")
	private String mContent;
	
	
	public Message() {
		mTo = new ArrayList<>();
	}
	
	public Message(String from, String to, String subject, String msg) {
		mFrom = from;
		String[] recipients = to.split(";");
		mTo = new ArrayList<>(Arrays.asList(recipients));
		mDate =  new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z").format(Calendar.getInstance().getTime());
		mSubject = subject;
		mContent = msg;
	}


	public String getFrom() {
		return mFrom;
	}


	public ArrayList<String> getTo() {
		return mTo;
	}


	public String getDate() {
		return mDate;
	}


	public String getSubject() {
		return mSubject;
	}


	public String getContent() {
		return mContent;
	}


	@Override
	public String toString() {
		return "Subject: "+mSubject+" To: "+mTo;
	}
	
}
