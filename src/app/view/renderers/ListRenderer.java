package app.view.renderers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;

import app.model.Message;
import app.util.Util;
 
public class ListRenderer extends JLabel implements ListCellRenderer<Message> {
 
    public ListRenderer() {
        setOpaque(true);
    }
 
    @Override
    public Component getListCellRendererComponent(JList<? extends Message> list, Message message, int index,
            boolean isSelected, boolean cellHasFocus) {
    	
        ImageIcon imageIcon = Util.loadIcon(ListRenderer.this.getClass(), "path24.png");
 
        setIcon(imageIcon);
        setText("Subject: "+message.getSubject());
 
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
 
        return this;
    }
} 