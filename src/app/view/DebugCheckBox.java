package app.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;

import app.Application;

public class DebugCheckBox extends JCheckBoxMenuItem {

	public DebugCheckBox() {
		// TODO Auto-generated constructor stub
	}

	public DebugCheckBox(Icon icon) {
		super(icon);
		// TODO Auto-generated constructor stub
	}

	public DebugCheckBox(String text) {
		super(text);
		addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Application.getInstance().getContext().setInDebugMode(isSelected());
			}
		});
	}

	public DebugCheckBox(Action a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	public DebugCheckBox(String text, Icon icon) {
		super(text, icon);
	}

	public DebugCheckBox(String text, boolean b) {
		super(text, b);
		// TODO Auto-generated constructor stub
	}

	public DebugCheckBox(String text, Icon icon, boolean b) {
		super(text, icon, b);
		// TODO Auto-generated constructor stub
	}

}
