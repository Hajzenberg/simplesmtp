package app.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;

import app.Application;
import app.listeners.FrameAdapter;
import app.listeners.ListListener;
import app.model.Message;
import app.util.Util;
import app.view.panels.MessagePanel;
import app.view.panels.SendPanel;
import app.view.renderers.ListRenderer;

public class ClientFrame extends JFrame {

	public static int WIDTH = 1280;
	public static int HEIGHT = 900;

	private JSplitPane mSplitPane;
	private JScrollPane mScrollPane;
	private SendPanel mSendPanel;
	
	private DefaultListModel<Message> mSentTabelModel;

	public ClientFrame() {
		setSize(new Dimension(WIDTH, HEIGHT));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener(new FrameAdapter());
		setTitle("Simple SMTP Client");
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
		setIconImage(Util.loadIcon(getClass(), "email.png").getImage());
		initUI();
		setResizable(false);
		setVisible(true);
	}

	private void initUI() {
		
		mScrollPane = initSentPanel();
		mScrollPane.setPreferredSize(new Dimension(600, 200));
		
		mSendPanel = new SendPanel(mSentTabelModel);

		mSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, mScrollPane, mSendPanel);
		mSplitPane.setDividerLocation(600);
		
		add(mSplitPane, BorderLayout.CENTER);
		setJMenuBar(new ClientMenuBar());
	}

	private JScrollPane initSentPanel() {

		JPanel mMessageListPanel = new JPanel();
		mMessageListPanel.setLayout(new BorderLayout());

		JList<Message> sentList = new JList<>();
		sentList.setFixedCellHeight(40);
		
		mSentTabelModel = new DefaultListModel<Message>();
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				ArrayList<Message> messages = Application.getInstance().getContext().getSent();
				
				for (int i=messages.size()-1; i >= 0 ; i--){
					mSentTabelModel.add(0,messages.get(i));
				}
				
				messages.removeAll(messages);
				
			}
		});
		
		sentList.setModel(mSentTabelModel);
		sentList.addMouseListener(new ListListener(sentList));
		sentList.setCellRenderer(new ListRenderer());
		
		
		mMessageListPanel.add(sentList, BorderLayout.CENTER);
	

		return new JScrollPane(mMessageListPanel);
	}

	public JSplitPane getSplitPane() {
		return mSplitPane;
	}

	public DefaultListModel<Message> getSentTabelModel() {
		return mSentTabelModel;
	}
	
	public void showMessage(Message message){
		mSplitPane.setRightComponent(new MessagePanel(false, message));
		revalidate();
		repaint();
	}
	
	public void showMessages(){
		mSplitPane.setRightComponent(mSendPanel);
		revalidate();
		repaint();
	}

}
