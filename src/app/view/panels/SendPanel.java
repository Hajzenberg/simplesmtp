package app.view.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import app.Application;
import app.controller.send.SendController;
import app.model.Message;

public class SendPanel extends JPanel {

	private JTextField mFromField;
	private JTextField mToField;
	private JTextField mSubjectField;
	private JTextArea mTextArea;
	private JButton mSendButton;

	public SendPanel(DefaultListModel<Message> sentTableModel) {

		setLayout(new BorderLayout());

		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new GridLayout(3, 1));

		// from

		JPanel fromPanel = new JPanel();

		JLabel fromLabel = new JLabel("From:");
		fromLabel.setPreferredSize(new Dimension(120, 26));
		mFromField = new JTextField(30);
		mFromField.setEnabled(false);

		fromPanel.add(fromLabel);
		fromPanel.add(mFromField);

		inputPanel.add(fromPanel);

		// to

		JPanel toPanel = new JPanel();

		JLabel toLabel = new JLabel("To:");
		toLabel.setPreferredSize(new Dimension(120, 26));
		mToField = new JTextField(30);
		//mToField.setText("kojadinovicdragoljub@gmail.com");

		toPanel.add(toLabel);
		toPanel.add(mToField);

		inputPanel.add(toPanel);

		// subject

		JPanel subjectPanel = new JPanel();

		JLabel subjectLabel = new JLabel("Subject: ");
		subjectLabel.setPreferredSize(new Dimension(120, 26));
		mSubjectField = new JTextField(30);

		subjectPanel.add(subjectLabel);
		subjectPanel.add(mSubjectField);

		inputPanel.add(subjectPanel);

		add(inputPanel, BorderLayout.NORTH);

		// area
		JPanel messagePanel = new JPanel();

		mTextArea = new JTextArea();
		mTextArea.setFont(mSubjectField.getFont());

		mTextArea.setPreferredSize(new Dimension(621, 600));
		JScrollPane scrollPane = new JScrollPane(mTextArea);
		mTextArea.setBorder(BorderFactory.createLineBorder(new Color(25, 25, 112), 1));

		messagePanel.add(scrollPane);

		add(messagePanel);

		JPanel buttonPanel = new JPanel();

		mSendButton = new JButton(new SendController(sentTableModel, this));

		buttonPanel.add(mSendButton);

		add(buttonPanel, BorderLayout.SOUTH);
	}

	public JTextField getFromField() {
		return mFromField;
	}

	public JTextField getToField() {
		return mToField;
	}

	public JTextField getSubjectField() {
		return mSubjectField;
	}

	public JTextArea getTextArea() {
		return mTextArea;
	}

	public JButton getSendButton() {
		return mSendButton;
	}
	
	
}
