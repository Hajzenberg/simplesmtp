package app.view;

import java.awt.CheckboxMenuItem;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import app.controller.ExitController;
import app.controller.SettingsController;
import app.util.Util;

public class ClientMenuBar extends JMenuBar {

	public ClientMenuBar() {
		JMenu file = new JMenu("File");
		
		file.add(new JMenuItem(new ExitController()));
		
		JMenu settings = new JMenu("Settings");
		
		settings.add(new JMenuItem(new SettingsController()));
		
		JMenu debug = new JMenu("Debug");
		
		debug.add(new DebugCheckBox("Debug"));
		
		add(file);
		add(settings);
		add(debug);
	}
	
}
