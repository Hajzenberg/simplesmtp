package app.controller;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

import javax.swing.AbstractAction;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.Application;
import app.model.Context;
import app.model.Message;
import app.util.Util;

public class ExitController extends AbstractAction {

	public ExitController() {
		putValue(NAME, "Exit");
		putValue(SMALL_ICON, Util.loadIcon(getClass(), "error32.png"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Enumeration<Message> enummeration = Application.getInstance().getClientFrame().getSentTabelModel().elements();

		while (enummeration.hasMoreElements()) {
			Application.getInstance().getContext().getSent().add(enummeration.nextElement());
		}

		ObjectMapper mapper = new ObjectMapper();
		mapper.setVisibility(PropertyAccessor.SETTER, JsonAutoDetect.Visibility.NONE);
		mapper.setVisibility(PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
		mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
		mapper.setVisibility(PropertyAccessor.IS_GETTER, JsonAutoDetect.Visibility.NONE);
		try {
			mapper.writeValue(new File(Context.CONTEXT_PATH), Application.getInstance().getContext());
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		Application.getInstance().getClientFrame().dispose();

	}

}
